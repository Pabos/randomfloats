from fastapi import FastAPI
from api.routes import router as random_routes

app = FastAPI()
app.include_router(random_routes, prefix="/random", tags=["random"])
