from fastapi import APIRouter
from fastapi import Body, Query
from fastapi.responses import JSONResponse
from api import lib
import string
import numpy as np
from typing import Dict
from models import Sentence


router = APIRouter()


@router.post("/simple/")
async def simple_generator_api(sentence: Sentence = Body(...)) -> list:
    random_array = await lib.simple_generator()
    return random_array


@router.post("/not_simple/")
async def not_simple_generator_api(sentence: Sentence = Body(...)) -> list:
    random_array = await lib.not_simple_generator(sentence=sentence.text)
    return random_array


@router.post("/hash/{sentence}")
async def using_hash_api(sentence: str = Query(...)) -> list:
    random_array = await lib.hash_generator(sentence=sentence)
    return random_array
