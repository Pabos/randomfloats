import string
import numpy as np
import structlog

log = structlog.get_logger()


async def simple_generator() -> list:
    """Generates 500 random floats between 0 and 100.

    Returns:
        list: [description]
    """
    return list(np.random.uniform(0, 100, size=500))


async def not_simple_generator(sentence: str) -> list:
    """Generates a list of 500 floats using the given sentence.
    The same sentence will always generate the same output, case matters.

    Args:
        sentence (str): Any sentence.

    Returns:
        list: 500 random floats between 0 and 100
    """
    base: str = " " + string.ascii_letters + string.punctuation + string.digits
    seed: int = 1
    sentence_val: int = 0

    sdict = {}
    for n, char in enumerate(base, start=1):
        sdict[char] = n

    # Maximum allowed value of seed
    max_seed = 2 ** 32 - 1

    for char in sentence:
        try:
            val = sdict[char]
            seed = val * seed
            sentence_val += val
        except ValueError as e:
            log.error(f"Invalid character used: {char}", exc_info=e)
            continue

    # Long sentences can have seeds which are too high
    if seed > max_seed:
        seed = max_seed - sentence_val

    np.random.seed(seed)
    random_array = np.random.uniform(0, 100, size=500)

    return list(random_array)


async def hash_generator(sentence: str) -> list:
    """Generates a list of 500 floats using hash of the given sentence.
    The same sentence will always generate the same output, case matters.

    Args:
        sentence (str): Any sentence.

    Returns:
        list: 500 random floats between 0 and 100
    """
    base_hash = hash(sentence)

    # Need hash to be a positive number
    strhash = str(base_hash if base_hash > 0 else -1 * base_hash)

    seeds = [
        int(strhash[0:5]),
        int(strhash[5:10]),
        int(strhash[10:15]),
        int(strhash[15:]),
    ]
    random_array = []

    for seed in seeds:
        np.random.seed(seed)
        random_array += list(np.random.uniform(0, 100, size=125))

    # Maximum allowed value of seed

    return random_array
