from fastapi.testclient import TestClient
from main import app
import json

client = TestClient(app)

def test_simple():
    response = client.post("/random/simple/", json={"text": "Hello there, general Kenobi."})
    assert response.status_code == 200
    assert len(json.loads(response.content)) == 500


def test_hash():
    response = client.post("/random/hash/hey mate wazzup")
    assert response.status_code == 200
    assert len(json.loads(response.content)) == 500

def test_not_simple():
    response = client.post("/random/not_simple/", json={"text": "Hello there, general Kenobi."})
    assert response.status_code == 200
    assert len(json.loads(response.content)) == 500

