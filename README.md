# RandomFloats

Simple app that has 3 API calls. Each of them takes a string as input and generates a list of 500 random floats (the floats are > 0 and < 100). They only differ in the manner of how they generate the random floats.

## Simple
The simplest solution. Completely ignores the input string sentence and generates a list of completely random 500 floats. Two calls with the same sentence will result in different outcomes.

Takes a body parameter in form of:
{"text": "My sentence"}

## Not so simple
Here the input sentence becomes important. Same sentence will always produce the same solution. The mechanism uses a list of all allowed characters (upper and lower case letters, whitespace, punctuation and digits). It iterates over the sentence and looks for position of each character in the list. So for example:

Sentence: "A brave new world."
The base string: "ABCDEFGH...abcdefgh... ...!?,...12345] (shortened version)

The sentence begins with "A", and its position in the base string is 1 (pythonically, it would be 0, but we start our positioning at 1). 

Once position is acquired for all characters, these numbers are multiplied together and the resulting value is used as seed for numpy.random.uniform() function.

Takes a body parameter in form of:
{"text": "My sentence"}

## Using hash
This one uses the python hash() function, which takes a string as input and outputs a 19 digit number. It splits the 19 digits into 4 numbers, first 3 numbers will have 5 digits, the last one (4th) will have 4 digits. And then it uses these as seeds for numpy.random.uniform().

Example:
Sentence: "Captain Marvel is the lamest Avenger."
Hash: 1177333743794635319
Seed 1: 11773
Seed 2: 33743
Seed 3: 79463
Seed 4: 5319

Each seed is used to generate 1/4 of the 500 floats (e.g. 125).

Takes a query parameter.
