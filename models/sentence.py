from pydantic import BaseModel, Field


class Sentence(BaseModel):
    text: str = Field(
        min_length=2,
        max_length=200,
        description="Input any sentence no longer than 200 characters.",
        title="A sentence",
    )
